import asyncio
from discord import Game, Member, Client
from discord import utils
from discord.ext.commands import Bot
import config


client = Bot(command_prefix=config.BOT_PREFIX)


@client.command(name='roles', pass_context=True)
async def my_roles(context):
    member: Member = context.message.author
    roles = list()
    for role in member.roles:
        if not role.is_everyone:
            roles.append(role.name)
    await client.say("Your active roles are {}".format(','.join(roles)))


@client.command(pass_context=True)
async def sync(context):
    """
    Syncs the calling user's roles with destination server
    """
    await sync_roles(context.message.author)
    await client.say("Done, your roles are synced.")


@client.command(pass_context=True)
async def check_admin(context):
    await is_admin(context.message.author)


@client.command(pass_context=True)
async def scan(context):
    fixed = 0
    if not await is_admin(context.message.author):
        print(f"{context.message.author} attempted to use restricted command !scan")
        await client.say(f"<@!{context.message.author.id}> Access denied.")
        return
    src_members = client.get_server(config.SOURCE_ID).members
    dest_members = client.get_server(config.DEST_ID).members

    if src_members is None or dest_members is None:
        print("Error: Sync failed: not connected to both servers.")
        return

    for member in dest_members:
        member_name = member.name.encode('utf-8')
        # for each member in the destination discord, ensure roles are synced
        src_match = utils.get(src_members, id=member.id)
        if src_match is None:
            print(f"Warning: user {member.name} is not on the source discord.")
            continue
        for role in src_match.roles:
            if role.name in config.SYNC_ROLES and not utils.get(member.roles, name=role.name):
                print(f"User {member_name} does not have {role.name}. Fixing.")
                await client.add_roles(member, utils.get(client.get_server(config.DEST_ID).roles,
                                                         name=role.name))
                fixed += 1
    await client.say(f"Server scanned and {fixed} roles fixed.")


@client.event
async def on_member_join(member: Member):
    member_name = member.name.encode('utf-8')
    if not member.server.id == config.DEST_ID:
        print(f"debug: member {member_name} did not join destination (server ID: {member.server.id}")
        return

    print(f"Join detected: {member_name}")
    await sync_roles(member)


@client.event
async def on_ready():
    await client.change_presence(game=Game(name="https://sosarp.net"))
    print("Logged in as " + client.user.name)
    if client.get_server(config.SOURCE_ID) is None or client.get_server(config.DEST_ID) is None:
        print("Warning: client not connected on both servers. Sync will not function.")


async def sync_roles(member):
    src_member = utils.get(client.get_server(config.SOURCE_ID).members, name=member.name)
    dest_member = utils.get(client.get_server(config.DEST_ID).members, name=member.name)
    dest_name = client.get_server(config.DEST_ID).name

    for role in src_member.roles:
        if role.name in config.SYNC_ROLES:
            await client.add_roles(dest_member, utils.get(client.get_server(config.DEST_ID).roles,
                                                          name=role.name))
            print(f"Added {role.name} to {dest_member} on server {dest_name}")


async def user_has_role(member, role_name):
    for role in member.roles:
        if role.name == role_name:
            return True
    return False


async def user_exists(members, name):
    for member in members:
        if str(member) == name:
            return member
    return None


async def is_admin(member):
    perms = member.server_permissions
    if perms.administrator is not None:
        return perms.administrator
    else:
        return False


loop = asyncio.get_event_loop()
loop.create_task(client.start(config.TOKEN))
loop.run_forever()
